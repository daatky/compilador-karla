/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;

/**
 *
 * @author andremontoya
 */
public class Parser {

    ArrayList<Token> tokens;
    ArrayList<Error> errores;
    String[] lineas;
    int contadorBrackets;
    

    public Parser() {
        this.tokens = new ArrayList<>();
        this.errores = new ArrayList<>();
        this.lineas = null;
        this.contadorBrackets = 0;
    }

    public void inicializar() {
        this.tokens = new ArrayList<>();
        this.errores = new ArrayList<>();
        this.contadorBrackets = 0;
        this.lineas = null;
    }

    public ArrayList<Token> getTokens() {
        return tokens;
    }

    public void setTokens(ArrayList<Token> tokens) {
        this.tokens = tokens;
    }

    public String[] getLineas() {
        return lineas;
    }

    public void setLineas(String[] lineas) {
        this.lineas = lineas;
    }

    public ArrayList<Error> getErrores() {
        return errores;
    }

    public void setErrores(ArrayList<Error> errores) {
        this.errores = errores;
    }

    public int getContadorBrackets() {
        return contadorBrackets;
    }

    public void setContadorBrackets(int contadorBrackets) {
        this.contadorBrackets = contadorBrackets;
    }

    private void addErrorToList(int numero, Token token, String mensaje, int estado) {
        Error error = new Error();
        error.setNumeroLinea(numero);
        error.setToken(token); //Token en validacion
        error.setError(mensaje);
        error.setEstado(estado);
        this.errores.add(error);
    }

    //Reglas de produccion
    String tipo = "TENTERO | TDOUBLE | TFLOAT | TSTRING | TBOOLJ | TBOOLS | TVAR | TLET";
    
    /*La logica para validar con cada regla de produccion (metodo) es repetitiva, se activa las banderas en el orden que cada regla indica y se valida que la sintaxis coincida*/
    /*El unico metodo que tiene algo adicional es condicional(), que al encontrar una llave de apertura {, aumenta en 1 la variable contadorBrackets, y si encuentra una llave de cerradura
    reduce en uno la misma variable. Al final en la vista principal, se valida que esta variable sea igual a 0, eso indica que no existen llaves sin cerrar en el codigo*/
    public int programa(int banderaCodigo) {
        Lexer lexer = new Lexer(); //Se inicializa el objeto lexer, para obtener los tokens de cada linea de codigo en analisis
        //codigo | condicional
        int i = 0, compilado = 0; //i indica el numero de linea, compilado indica si el codigo esta correcto o no
        boolean banderaError = false; //Bandera de error, si se activa el ciclo while se detiene y retorna el error
        while (i < this.lineas.length && banderaError == false) { //El ciclo se ejecuta mientras haya lineas que analizar y no se haya detectado errores en las mismas
            lexer.inicializar(); //Se inicializa el objeto lexer, cada vez que se analiza una nueva linea
            lexer.analizar(this.lineas[i]); //Se analiza la linea de codigo para sacar sus tokens
            ArrayList<Token> tokensLinea = lexer.getTokens(); //Se recupera los tokens de la linea en analisis
            if (tokensLinea.size() > 0) { //Si la liena contiene tokens que no sean solo espacios en blanco se analiza, caso contrario se salta
                //status -1 = hay  error sintactico en la liena, 0 = no hay coincidencias  o 1 = hay coincidencia y no existen errores.
                int statusCodigo = codigo(i + 1, this.lineas[i], tokensLinea); //Se ejecuta la primera regla de produccion
                System.out.println("El status de la linea " + this.lineas[i] + " en codigo() es: " + statusCodigo);

                if (statusCodigo != -1) { //Si no existio errores al analir la liena con codigo()
                    if (statusCodigo == 0) { //Si no se encontro coincidencias en codigo()
                        //Se procede a validar con condicional()
                        int statusCondicional = condicional(i + 1, this.lineas[i], tokensLinea, banderaCodigo);
                        System.out.println("El status de la linea " + this.lineas[i] + " en condicional() es: " + statusCondicional);
                        if (statusCondicional == 1) {
                            compilado = 1;
                        } else {
                            compilado = -1;
                            banderaError = true;
                        }
                    } else {
                        compilado = 1;
                    }
                } else {
                    compilado = -1;
                    banderaError = true;
                }
            }
            i++;
        }
        return compilado;
    }

    private int codigo(int numero, String linea, ArrayList<Token> tokensLinea) {
        //codigo: varDeclaracion | varAsignacion =>Regla de producción
        int status; //-1 error de sintaxis, 0 no coincide con esa regla, 1 coincide y correcta

        status = varDeclaracion(numero, linea, tokensLinea); //Se ejecuta las reglas de producción de izquierda a derecha
        System.out.println("El status de varDeclaracion es: " + status);
        if (status == 0) {
            status = varAsignacion(numero, linea, tokensLinea);
            System.out.println("El status de varAsignacion es: " + status);
            return status; //Puede ser -1 0 1
        } else {
            return status; //Puede ser -1 0 1
        }
    }
    
    private int varDeclaracion(int numero, String linea, ArrayList<Token> tokensLinea) {
        //varDeclaracion: tipo ID FINL | tipo ID ASIGNADOR VALOR FINL => Regla de producción
        int status = 0; //valores validos -1, 0, 1
        boolean banderaStop = false; //Bandera que detiene el ciclo si encontro errores o encontro una coincidencia
       
        //Cada una de estas banderas a continuación deben activarse en orden a como indica las regla de produccion para este metodo,
        //lo cual determina si existe una coincidencia o no
        boolean Tipo = false, ID = false, ASIGNADOR = false, VALOR = false, FINL = false; 
        
        int i = 0;
        while (i < tokensLinea.size() && banderaStop == false) { //Mientras haya tokens y no haya errores o coincidencia
            Token token = tokensLinea.get(i); //Se obtiene el token de la lista en la posicion i
            Token tokenAUX = null; //Token auxiliar que ayuda a validar el token siguiente del que este en la posicion i
            //tipo
            CharSequence s = "" + token.getTipo();
            if (tipo.contains(s)) { //Si el token coincide con tipo
                if (i == 0) { //Acorde a las reglas de produccion debe ser el primer token de la linea
                    //Se valida hacia adelante
                    tokenAUX = tokensLinea.get(i + 1); //Se obtiene el token siguiente
                    if (!(tokenAUX.getTipo().toString().equals("ID"))) { //Se valida que token debe estar a continuacion de tipo para activar las banderas o indicar error
                        addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                        //Avisar del error
                        status = -1; //Valor a devolver
                        banderaStop = true;
                    } else {
                        //No hay error, se procede a activar las banderas Tipo y ID
                        Tipo = true;
                        ID = true;
                    }
                }
            } else {
                if (token.getTipo().toString().equals("ID")) { //Si el token coincide con ID
                    if (Tipo && ID && i > 0) { //Para que sea valido segun las reglas de produccion, ID no debe ser el primer token de la linea, es decir las banderas Tipo y ID deben estar activas y i > 0
                        //System.out.println("alert 3 - hay tipo y ID");
                        //Ya se que el segundo token de la linea es un ID
                        tokenAUX = tokensLinea.get(i + 1); //Se obtiene el token siguiente
                        if (tokenAUX.getTipo().toString().equals("FINL")) { //se valida 
                            FINL = true;
                        } else {
                            if (tokenAUX.getTipo().toString().equals("ASIGNADOR")) { //Se valida que token debe estar a continuacion de tipo para activar las banderas
                                ASIGNADOR = true;
                            }
                        }
                    } else { //Si las banderas no estan activas, no hay coincidencia con esta regla de produccion
                        //Vaciar la lista de errores, porque no hay conincidencia con esta regla de produccion
                        this.errores = new ArrayList<>();
                        banderaStop = true; //Paramos el ciclo de validacion porque no existe coincidencias con esta regla
                    }
                } else {
                    if (token.getTipo().toString().equals("ASIGNADOR")) {
                        //System.out.println("alert 4 - hay tipo y ID y ASIGNADOR");
                        if (Tipo && ID && ASIGNADOR && i > 0) {
                            tokenAUX = tokensLinea.get(i + 1);
                            if (tokenAUX.getTipo().toString().equals("VALOR")) {
                                VALOR = true;
                            } else {
                                addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                //Avisar del error
                                status = -1; //Valor a devolver
                                banderaStop = true;
                            }
                        }
                    } else {
                        if (token.getTipo().toString().equals("VALOR")) {
                            //System.out.println("alert 5 - hay tipo y ID y ASIGNADOR y  VALOR");
                            if (Tipo && ID && ASIGNADOR && VALOR && i > 0) {
                                tokenAUX = tokensLinea.get(i + 1);
                                if (tokenAUX.getTipo().toString().equals("FINL")) {
                                    FINL = true;
                                } else {
                                    addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                    //Avisar del error
                                    status = -1; //Valor a devolver
                                    banderaStop = true;
                                }
                            }
                        } else {
                            if (token.getTipo().toString().equals("FINL")) {
                                //System.out.println("alert 5 - hay tipo y ID y ASIGNADOR y  VALOR y FINL || tipo ID FINl");
                                if ((Tipo && ID && ASIGNADOR && VALOR && FINL && i > 0) || (Tipo && ID && FINL && i > 0)) {
                                    //Ya se que es un fin de linea
                                    if ((i + 1) < tokensLinea.size()) {
                                        tokenAUX = tokensLinea.get(i + 1);
                                        addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                        //Avisar del error
                                        status = -1; //Valor a devolver
                                        banderaStop = true;
                                    } else {
                                        status = 1;
                                        banderaStop = true;
                                    }
                                } else {
                                    //System.out.println("no hay coincidencia");
                                    //Vaciar la lista de errores, porque no hay conincidencia con esta regla de produccion
                                    this.errores = new ArrayList<>();
                                    banderaStop = true; //Paramos el ciclo de validacion porque no existe coincidencias con esta regla
                                }
                            }
                        }
                    }
                }
            }
            i++;
        }
        return status;
    }

    private int varAsignacion(int numero, String linea, ArrayList<Token> tokensLinea) {
        //ID ASIGNADOR VALOR FINL
        int status = 0; //valores validos -1, 0, 1
        boolean banderaStop = false;
        boolean ID = false, ASIGNADOR = false, VALOR = false, FINL = false;
        int i = 0;
        while (i < tokensLinea.size() && banderaStop == false) {
            Token token = tokensLinea.get(i);
            Token tokenAUX = null;
            
            if (token.getTipo().toString().equals("ID")) {
                if (i == 0) {
                    tokenAUX = tokensLinea.get(i + 1);
                    if (tokenAUX.getTipo().toString().equals("ASIGNADOR")) {
                        ID = true;
                        ASIGNADOR = true;
                    } else {
                        addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                        //Avisar del error
                        status = -1; //Valor a devolver
                        banderaStop = true;
                    }
                }
            } else {
                if (token.getTipo().toString().equals("ASIGNADOR")) {
                    if (ID && ASIGNADOR && i > 0) {
                        tokenAUX = tokensLinea.get(i + 1);
                        if (tokenAUX.getTipo().toString().equals("VALOR")) {
                            VALOR = true;
                        } else {
                            addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                            //Avisar del error
                            status = -1; //Valor a devolver
                            banderaStop = true;
                        }
                    }
                } else {
                    if (token.getTipo().toString().equals("VALOR")) {
                        if (ID && ASIGNADOR && VALOR && i > 0) {
                            tokenAUX = tokensLinea.get(i + 1);
                            if (tokenAUX.getTipo().toString().equals("FINL")) {
                                FINL = true;
                            } else {
                                addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                //Avisar del error
                                status = -1; //Valor a devolver
                                banderaStop = true;
                            }
                        }
                    } else {
                        if (token.getTipo().toString().equals("FINL")) {
                            if (ID && ASIGNADOR && VALOR && FINL && i > 0) {
                                //Ya se que es un fin de linea
                                if ((i + 1) < tokensLinea.size()) {
                                    tokenAUX = tokensLinea.get(i + 1);
                                    addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                    //Avisar del error
                                    status = -1; //Valor a devolver
                                    banderaStop = true;
                                } else {
                                    status = 1;
                                    banderaStop = true;
                                }
                            } else {
                                //Vaciar la lista de errores, porque no hay conincidencia con esta regla de produccion
                                this.errores = new ArrayList<>();
                                banderaStop = true; //Paramos el ciclo de validacion porque no existe coincidencias con esta regla
                            }
                        }
                    }
                }
            }
            i++;
        }
        return status;
    }

    private int condicional(int numero, String linea, ArrayList<Token> tokensLinea, int banderaCodigo) {
        //varDeclaracion | varAsignacion
        int status; //-1 error de sintaxis, 0 no coincide con esa regla, 1 coincide y correcta

        if (banderaCodigo == 0) { // 0 = JAVA
            status = condicionalIFA(numero, linea, tokensLinea);
        } else {  // 1 = SWIFT
            status = condicionalIFAS(numero, linea, tokensLinea);
        }
        System.out.println("El status de condicionalIFA es: " + status);
        if (status == 0) {
            status = condicionalIFB(numero, linea, tokensLinea);
            System.out.println("El status de condicionalIFB es: " + status);
            if (status == 0) {
                status = condicionalIFC(numero, linea, tokensLinea);
                System.out.println("El status de condicionalIFC es: " + status);
                return status;
            } else {
                return status; //Puede ser -1 0 1
            }
        } else {
            return status; //Puede ser -1 0 1
        }
    }

    private int condicionalIFA(int numero, String linea, ArrayList<Token> tokensLinea) {
        //CONIF OPENP ID COMPARADOR VALOR CLOSEP OPENB
        int status = 0; //valores validos -1, 0, 1
        boolean banderaStop = false;
        boolean CONIF = false, OPENP = false, ID = false, COMPARADOR = false, VALOR = false, CLOSEP = false, OPENB = false;
        int i = 0;
        while (i < tokensLinea.size() && banderaStop == false) {
            Token token = tokensLinea.get(i);
            Token tokenAUX = null;
            if (token.getTipo().toString().equals("CONIF")) {
                if (i == 0) {
                    tokenAUX = tokensLinea.get(i + 1);
                    if (tokenAUX.getTipo().toString().equals("OPENP")) {
                        CONIF = true;
                        OPENP = true;
                    } else {
                        addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                        //Avisar del error
                        status = -1; //Valor a devolver
                        banderaStop = true;
                    }
                }
            } else {
                if (token.getTipo().toString().equals("OPENP")) {
                    if (CONIF && OPENP && i > 0) {
                        tokenAUX = tokensLinea.get(i + 1);
                        if (tokenAUX.getTipo().toString().equals("ID")) {
                            ID = true;
                        } else {
                            addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                            //Avisar del error
                            status = -1; //Valor a devolver
                            banderaStop = true;
                        }
                    }
                } else {
                    if (token.getTipo().toString().equals("ID")) {
                        if (CONIF && OPENP && ID && i > 0) {
                            tokenAUX = tokensLinea.get(i + 1);
                            if (tokenAUX.getTipo().toString().equals("COMPARADOR")) {
                                COMPARADOR = true;
                            } else {
                                addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                //Avisar del error
                                status = -1; //Valor a devolver
                                banderaStop = true;
                            }
                        }
                    } else {
                        if (token.getTipo().toString().equals("COMPARADOR")) {
                            if (CONIF && OPENP && ID && COMPARADOR && i > 0) {
                                tokenAUX = tokensLinea.get(i + 1);
                                if (tokenAUX.getTipo().toString().equals("VALOR")) {
                                    VALOR = true;
                                } else {
                                    addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                    //Avisar del error
                                    status = -1; //Valor a devolver
                                    banderaStop = true;
                                }
                            }
                        } else {
                            if (token.getTipo().toString().equals("VALOR")) {
                                if (CONIF && OPENP && ID && COMPARADOR && VALOR && i > 0) {
                                    tokenAUX = tokensLinea.get(i + 1);
                                    if (tokenAUX.getTipo().toString().equals("CLOSEP")) {
                                        CLOSEP = true;
                                    } else {
                                        addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                        //Avisar del error
                                        status = -1; //Valor a devolver
                                        banderaStop = true;
                                    }
                                }
                            } else {
                                if (token.getTipo().toString().equals("CLOSEP")) {
                                    if (CONIF && OPENP && ID && COMPARADOR && VALOR && CLOSEP && i > 0) {
                                        tokenAUX = tokensLinea.get(i + 1);
                                        if (tokenAUX.getTipo().toString().equals("OPENB")) {
                                            this.contadorBrackets = this.contadorBrackets + 1;
                                            System.out.println("Contador braces update: " + this.contadorBrackets);
                                            OPENB = true;
                                        } else {
                                            addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                            //Avisar del error
                                            status = -1; //Valor a devolver
                                            banderaStop = true;
                                        }
                                    }
                                } else {
                                    if (token.getTipo().toString().equals("OPENB")) {
                                        if (CONIF && OPENP && ID && COMPARADOR && VALOR && CLOSEP && OPENB && i > 0) {
                                            if ((i + 1) < tokensLinea.size()) {
                                                tokenAUX = tokensLinea.get(i + 1);
                                                addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                                //Avisar del error
                                                status = -1; //Valor a devolver
                                                banderaStop = true;
                                            } else {
                                                status = 1;
                                                banderaStop = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            i++;
        }
        return status;
    }

    private int condicionalIFAS(int numero, String linea, ArrayList<Token> tokensLinea) {
        //CONIF ID COMPARADOR VALOR OPENB
        int status = 0; //valores validos -1, 0, 1
        boolean banderaStop = false;
        boolean CONIF = false, ID = false, COMPARADOR = false, VALOR = false, OPENB = false;
        int i = 0;
        while (i < tokensLinea.size() && banderaStop == false) {
            Token token = tokensLinea.get(i);
            Token tokenAUX = null;
            if (token.getTipo().toString().equals("CONIF")) {
                if (i == 0) {
                    tokenAUX = tokensLinea.get(i + 1);
                    if (tokenAUX.getTipo().toString().equals("ID")) {
                        CONIF = true;
                        ID = true;
                    } else {
                        addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                        //Avisar del error
                        status = -1; //Valor a devolver
                        banderaStop = true;
                    }
                }
            } else {
                if (token.getTipo().toString().equals("ID")) {
                    if (CONIF && ID && i > 0) {
                        tokenAUX = tokensLinea.get(i + 1);
                        if (tokenAUX.getTipo().toString().equals("COMPARADOR")) {
                            COMPARADOR = true;
                        } else {
                            addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                            //Avisar del error
                            status = -1; //Valor a devolver
                            banderaStop = true;
                        }
                    }
                } else {
                    if (token.getTipo().toString().equals("COMPARADOR")) {
                        if (CONIF && ID && COMPARADOR && i > 0) {
                            tokenAUX = tokensLinea.get(i + 1);
                            if (tokenAUX.getTipo().toString().equals("VALOR")) {
                                VALOR = true;
                            } else {
                                addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                //Avisar del error
                                status = -1; //Valor a devolver
                                banderaStop = true;
                            }
                        }
                    } else {
                        if (token.getTipo().toString().equals("VALOR")) {
                            if (CONIF && ID && COMPARADOR && VALOR && i > 0) {
                                tokenAUX = tokensLinea.get(i + 1);
                                if (tokenAUX.getTipo().toString().equals("OPENB")) {
                                    this.contadorBrackets = this.contadorBrackets + 1;
                                    System.out.println("Contador braces update: " + this.contadorBrackets);
                                    OPENB = true;
                                } else {
                                    addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                    //Avisar del error
                                    status = -1; //Valor a devolver
                                    banderaStop = true;
                                }
                            }
                        } else {
                            if (token.getTipo().toString().equals("OPENB")) {
                                if (CONIF && ID && COMPARADOR && VALOR && OPENB && i > 0) {
                                    if ((i + 1) < tokensLinea.size()) {
                                        tokenAUX = tokensLinea.get(i + 1);
                                        addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                        //Avisar del error
                                        status = -1; //Valor a devolver
                                        banderaStop = true;
                                    } else {
                                        status = 1;
                                        banderaStop = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            i++;
        }
        return status;
    }

    private int condicionalIFB(int numero, String linea, ArrayList<Token> tokensLinea) {
        //CLOSEB CONELSE OPENB
        int status = 0; //valores validos -1, 0, 1
        boolean banderaStop = false;
        boolean CLOSEB = false, CONELSE = false, OPENB = false;
        int i = 0;
        while (i < tokensLinea.size() && banderaStop == false) {
            Token token = tokensLinea.get(i);
            Token tokenAUX = null;
            if (token.getTipo().toString().equals("CLOSEB")) {
                if (i == 0 && (i + 1) < tokensLinea.size()) {
                    System.out.println("alert - hay CLOSEB");
                    tokenAUX = tokensLinea.get(i + 1);
                    if (tokenAUX.getTipo().toString().equals("CONELSE")) {
                        this.contadorBrackets = this.contadorBrackets - 1;
                        System.out.println("Contador braces update: " + this.contadorBrackets);
                        CLOSEB = true;
                        CONELSE = true;
                    } else {
                        addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                        //Avisar del error
                        status = -1; //Valor a devolver
                        banderaStop = true;
                    }
                }
            } else {
                if (token.getTipo().toString().equals("CONELSE")) {
                    System.out.println("alert - hay  CLOSEB CONELSE");
                    if (CLOSEB && CONELSE && i > 0) {
                        tokenAUX = tokensLinea.get(i + 1);
                        if (tokenAUX.getTipo().toString().equals("OPENB")) {
                            this.contadorBrackets = this.contadorBrackets + 1;
                            System.out.println("Contador braces update: " + this.contadorBrackets);
                            OPENB = true;
                        } else {
                            addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                            //Avisar del error
                            status = -1; //Valor a devolver
                            banderaStop = true;
                        }
                    }
                } else {
                    if (token.getTipo().toString().equals("OPENB")) {
                        System.out.println("alert - hay  CLOSEB CONELSE OPENB");
                        if (CLOSEB && CONELSE && OPENB && i > 0) {
                            if ((i + 1) < tokensLinea.size()) {
                                tokenAUX = tokensLinea.get(i + 1);
                                addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                                //Avisar del error
                                status = -1; //Valor a devolver
                                banderaStop = true;
                            } else {
                                status = 1;
                                banderaStop = true;
                            }
                        }
                    }
                }
            }
            i++;
        }
        return status;
    }

    private int condicionalIFC(int numero, String linea, ArrayList<Token> tokensLinea) {
        System.out.println("alert 0");
        //CLOSEB
        int status = 0; //valores validos -1, 0, 1
        boolean banderaStop = false;
        boolean CLOSEB = false;
        int i = 0;
        while (i < tokensLinea.size() && banderaStop == false) {
            Token token = tokensLinea.get(i);
            Token tokenAUX = null;
            if (token.getTipo().toString().equals("CLOSEB")) {
                if (i == 0) {
                    System.out.println("alert - hay CLOSEB");
                    if ((i + 1) < tokensLinea.size()) {
                        tokenAUX = tokensLinea.get(i + 1);
                        addErrorToList(numero, token, "Error en la línea " + numero + ", token inesperado '" + tokenAUX.getValor() + "' despues de " + token.getValor(), 1);
                        //Avisar del error
                        status = -1; //Valor a devolver
                        banderaStop = true;
                    } else {
                        this.contadorBrackets = this.contadorBrackets - 1;
                        System.out.println("Contador braces update: " + this.contadorBrackets);
                        CLOSEB = true;
                        status = 1;
                        banderaStop = true;
                    }
                }
            }
            i++;
        }
        return status;
    }

}
