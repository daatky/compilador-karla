/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.StringTokenizer;
import controlador.Token.Tipos;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author andremontoya
 */
public class Lexer {
    
    private ArrayList<Token> tokens;
    private ArrayList<String> errores;
    private StringTokenizer st;
    private boolean statusErrorCodigo;

    public Lexer() {
        this.tokens = new ArrayList<>();
        this.errores = new ArrayList<>();
        this.statusErrorCodigo = false;
    }
    
    public void inicializar(){
        this.tokens = new ArrayList<>();
        this.errores = new ArrayList<>();
        this.statusErrorCodigo = false;
    }

    public ArrayList<Token> getTokens() {
        return tokens;
    }

    public void setTokens(ArrayList<Token> tokens) {
        this.tokens = tokens;
    }

    public ArrayList<String> getErrores() {
        return errores;
    }

    public void setErrores(ArrayList<String> errores) {
        this.errores = errores;
    }

    public boolean isStatusErrorCodigo() {
        return statusErrorCodigo;
    }

    public void setStatusErrorCodigo(boolean statusErrorCodigo) {
        this.statusErrorCodigo = statusErrorCodigo;
    }
    
    
    
    public void analizar(String codigo) {
        this.st = new StringTokenizer(codigo); // Divide toda la cadena de Texto con el codigo ingresado, en una lista de n posiciiones segun el " " (espacio en blanco).
                                               // Cada posición de la lista es un token
        
        while(st.hasMoreTokens()) { //Mientras haya tokens que recorrer
            String palabra = st.nextToken(); // Se obtiene el token a analizar
            boolean matched = false; //Inidica si el token en validación existe dentro de la gramatica

            for (Tipos tokenTipo : Tipos.values()) { //Se recorre uno a uno lo tokens que pertenecen a la gramatica
                Pattern patron = Pattern.compile(tokenTipo.patron); //Se obtiene el patron de caracteres de la palabra en analisis
                Matcher matcher = patron.matcher(palabra); //Se valida si el patron obtenido es valido 
                if(matcher.find()) { //Si el patron obtenido es valido, se añade el token a la lista
                    Token tk = new Token();
                    tk.setTipo(tokenTipo);
                    tk.setValor(palabra);
                    tokens.add(tk);
                    matched = true; //Se activa la variable matched en true, que indica que el token es valido
                }
            }

            if (!matched) { //Si el token es invalido (no existe coincidencias), se inserta en la lista de tokens desconocidos
                statusErrorCodigo = true;
                this.errores.add(palabra);
            }
        }
    }
}
