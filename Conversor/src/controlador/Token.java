/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author Karla
 */
public class Token {

    public Tipos getTipo() {
        return tipo;
    }

    public void setTipo(Tipos tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private Tipos tipo;
    private String valor;

    enum Tipos {
    	CONIF ("(if)$"),
    	OPENP ("[(]$"),
    	CLOSEP ("[)]$"),
    	OPENB ("[{]$"),
    	CLOSEB ("[}]$"),
    	CONELSE ("(else)$"),
        COMPARADOR ("^(>|<|>=|<=|==)$"),
    	TENTERO ("(int)$"),
    	TDOUBLE ("(double)$"),
    	TFLOAT ("(float)$"),
    	TSTRING ("(String)$"),
    	TBOOL ("^(boolean|Bool)$"),
    	TVAR ("(var)$"),
        TLET ("(let)$"),
    	ASIGNADOR ("^=(?!=)$"),
    	ID ("^(?!String|int|double|float|boolean|Bool|var|if|else|elseif|true|false)[a-zA-Z][a-zA-Z0-9]*$"),
    	VALOR ("^([0-9]+(.[0-9])*|true|false|('[a-zA-Z]+'))$"),
    	FINL ("[;]$");
    
        public final String patron;
        Tipos(String s) {
            this.patron = s;
        }
    }

}
